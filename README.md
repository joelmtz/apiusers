# 1 - Servicio que permite la creación de un nuevo usuario:
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_PORT => "8000",
	  CURLOPT_URL => "http://127.0.0.1:8000/api/users/",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "username=joel_mtz&email=joel%40gmail.com&names=Joel&paternal_surname=Martinez&age=24&role=2&permissions=2%2C3",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache",
	    "content-type: application/x-www-form-urlencoded"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}


# 2 - Servicio que lista todos los usuarios existentes:
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_PORT => "8000",
	  CURLOPT_URL => "http://127.0.0.1:8000/api/users/",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}


# 3 - Servicio que lista todos los usuarios que pertenezcan a un rol determinado:
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_PORT => "8000",
	  CURLOPT_URL => "http://127.0.0.1:8000/api/users/roleid/2",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}


# 4 - Servicio que permita listar los usuarios que cuenten con algún permiso en particular:
	<?php

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_PORT => "8000",
	  CURLOPT_URL => "http://127.0.0.1:8000/api/users/permission/1",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}


# 5 , 6 - Servicio que permita lista a todos aquellos usuarios activos o inactivo ,sólo se cambia el id en la url |  1 = Activo, 0 = Inactivo  | :

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_PORT => "8000",
	  CURLOPT_URL => "http://127.0.0.1:8000/api/users/status/0",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}


# 7 - Servicio que permite la actualización de un usuario:

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_PORT => "8000",
	  CURLOPT_URL => "http://127.0.0.1:8000/api/users/1",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "PUT",
	  CURLOPT_POSTFIELDS => "username=joel_mtz&email=joel%40gmail.com&names=Joel&paternal_surname=Martinez&age=24&role=2&permissions=2%2C3",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache",
	    "content-type: application/x-www-form-urlencoded"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}


# Servicio que permit3 visualizar la información de un usuario dado su identificador:
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_PORT => "8000",
	  CURLOPT_URL => "http://127.0.0.1:8000/api/users/1",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}


# Servicio que permite la eliminación de un usuario:

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_PORT => "8000",
	  CURLOPT_URL => "http://127.0.0.1:8000/api/users/2",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "DELETE",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}