<?php

namespace App\Http\Controllers;

use App\Users;
use App\Roles;
use Illuminate\Http\Request;
use App\Http\Resources\Users as UserResource;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $m_respuesta = [];
        $e_codigo_error = 500;
        # Obtiene todos los usuarios existentes
        try {
            # Hace un left join a los permisos para obtener los nombres con find_in_set
            $m_usuarios_existentes = Users::
            select("users.id","users.username","users.email","users.names","users.paternal_surname","users.maternal_surname","users.age","roles.name AS role",\DB::raw("GROUP_CONCAT(permissions.name) as permissions"))->leftjoin("permissions",\DB::raw("FIND_IN_SET(permissions.id, users.permissions)"),">",\DB::raw("'0'"))->leftjoin("roles", "users.role", "=", "roles.id")->groupBy("users.id","users.username","users.email","users.names","users.paternal_surname","users.maternal_surname","users.age", "roles.name")->get();

            if ($m_usuarios_existentes->count() > 0) {
                $m_respuesta = ['status' => 'success', 'data' => $m_usuarios_existentes];
            }else{
                $m_respuesta = ['status' => 'error', 'message' => 'Oops! There is no registered user'];
            }
            $e_codigo_error = 201;
        } catch (\Illuminate\Database\QueryException $e) {
            # Si hubo algún error en el la inserción catchar y
            $m_respuesta = ['status' => 'error','code' => $e->errorInfo[1], 'message' => $e->errorInfo[2]];
        }
        
        return response()->json($m_respuesta, $e_codigo_error);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $m_respuesta = [];
        $e_codigo_error = 500;
        try {
            # Hace la instancia al modelo
            $o_usuario = new Users;
            # Setea los nuevos datos
            $o_usuario->username = $request->input('username');
            $o_usuario->email = $request->input('email');
            $o_usuario->names = $request->input('names');
            $o_usuario->paternal_surname = $request->input('paternal_surname');
            $o_usuario->maternal_surname = $request->has('maternal_surname') ? $request->input('maternal_surname') : null;
            $o_usuario->age = $request->has('age') ? $request->input('age') : null;
            $o_usuario->role = $request->input('role');
            $o_usuario->permissions = $request->input('permissions');
            $o_usuario->status = $request->has('status') ? $request->input('status') : 1;
            # Intenta guardar los datos, si surge algún error está controlado
            $o_usuario->save();   
            $e_codigo_error = 201;
            # Si no surge ningún error mandar el status success
            $m_respuesta = ['status' => 'success'];
        } catch (\Illuminate\Database\QueryException $e) {
            # Si hubo algún error en el la inserción catchar y
            $m_respuesta = ['status' => 'error','code' => $e->errorInfo[1], 'message' => $e->errorInfo[2]];
        }
        return response()->json($m_respuesta, $e_codigo_error);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function show(Users $users, $user)
    {
        $m_respuesta = [];
        $e_codigo_error = 500;
        try {
            $o_usuario = Users::where("id", "=", $user)->get();
            if ($o_usuario->count() > 0) {
                $m_respuesta = ['status' => 'success', 'data' => $o_usuario];
                $e_codigo_error = 201;
            }else{
                $m_respuesta = ['status' => 'error', 'message' => 'Oops! The client with id '.$user.' does not exist'];
            }
            
        } catch (\Illuminate\Database\QueryException $e) {
            # Si hubo algún error en el la inserción catchar y
            $m_respuesta = ['status' => 'error','code' => $e->errorInfo[1], 'message' => $e->errorInfo[2]];
        }
        return response()->json($m_respuesta, $e_codigo_error);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userid)
    {
        $m_respuesta = [];
        $e_codigo_error = 500;
        try {
            $o_usuario = Users::findOrFail($userid);
            $o_usuario->username = $request->input('username');
            $o_usuario->email = $request->input('email');
            $o_usuario->names = $request->input('names');
            $o_usuario->paternal_surname = $request->input('paternal_surname');
            $o_usuario->maternal_surname = $request->has('maternal_surname') ? $request->input('maternal_surname') : null;
            $o_usuario->age = $request->has('age') ? $request->input('age') : null;
            $o_usuario->role = $request->input('role');
            $o_usuario->permissions = $request->input('permissions');
            $o_usuario->status = $request->has('status') ? $request->input('status') : 1;
            # Intenta guardar los datos, si surge algún error está controlado
            $o_usuario->save();   
            $e_codigo_error = 201;
            # Si no surge ningún error mandar el status success
            $m_respuesta = ['status' => 'success'];
        } catch (\Illuminate\Database\QueryException $e) {
            # Si hubo algún error en el la inserción catchar y
            $m_respuesta = ['status' => 'error','code' => $e->errorInfo[1], 'message' => $e->errorInfo[2]];
        }
        return response()->json($m_respuesta, $e_codigo_error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy(Users $users, $user)
    {
        $m_respuesta = [];
        $e_codigo_error = 500;
        try {
            $o_elimina_usuario = Users::find($user);
            if ($o_elimina_usuario) {
                $o_elimina_usuario->delete();
                $m_respuesta = ['status' => 'success'];
                $e_codigo_error = 201;
            }else{
                $m_respuesta = ['status' => 'error', 'message' => 'Oops! The client with id '.$user.' does not exist'];
            }
        } catch (\Illuminate\Database\QueryException $e) {
            # Si hubo algún error en el la inserción catchar y
            $m_respuesta = ['status' => 'error','code' => $e->errorInfo[1], 'message' => $e->errorInfo[2]];
        }
        return response()->json($m_respuesta, $e_codigo_error);
    }

    public function getUserByRole($roleid){
        $m_respuesta = [];
        $e_codigo_error = 500;
        try {
            $o_usuario = Users::where('role', '=', $roleid)->get();
            if ($o_usuario->count() > 0) {
                $m_respuesta = ['status' => 'success', 'data' => $o_usuario];
            }else{
                $m_respuesta = ['status' => 'error', 'message' => 'UPS! No customer with role ID '.$roleid];
            }
            $e_codigo_error = 201;
        } catch (\Illuminate\Database\QueryException $e) {
            $m_respuesta = ['status' => 'error','code' => $e->errorInfo[1], 'message' => $e->errorInfo[2]];
        }
        return response()->json($m_respuesta, $e_codigo_error);
    }

    public function getUserByPermission($idpermission){
        $m_respuesta = [];
        $e_codigo_error = 500;
        try {
            $o_usuario = Users::whereRaw('FIND_IN_SET('.$idpermission.',users.permissions)')->get();
            if ($o_usuario->count() > 0) {
                $m_respuesta = ['status' => 'success', 'data' => $o_usuario];
            }else{
                $m_respuesta = ['status' => 'error', 'message' => 'UPS! No customer with permission ID '.$idpermission];
            }
            $e_codigo_error = 201;
        } catch (\Illuminate\Database\QueryException $e) {
            $m_respuesta = ['status' => 'error','code' => $e->errorInfo[1], 'message' => $e->errorInfo[2]];
        }
        return response()->json($m_respuesta, $e_codigo_error);
    }

    public function getUserbyStatus($status){
        $m_respuesta = [];
        $e_codigo_error = 500;
        try {
            $o_usuario = Users::where('status', '=', $status)->get();
            if ($o_usuario->count() > 0) {
                $m_respuesta = ['status' => 'success', 'data' => $o_usuario];
            }else{
                $m_respuesta = ['status' => 'error', 'message' => 'UPS! No customer with status ID '.$status];
            }
            $e_codigo_error = 201;
        } catch (\Illuminate\Database\QueryException $e) {
            $m_respuesta = ['status' => 'error','code' => $e->errorInfo[1], 'message' => $e->errorInfo[2]];
        }
        return response()->json($m_respuesta, $e_codigo_error);
    }
}
