<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('users/roleid/{roleid}', 'UsersController@getUserByRole');
Route::get('users/permission/{idpermission}', 'UsersController@getUserByPermission');
Route::get('users/status/{status}', 'UsersController@getUserByStatus');
Route::ApiResource('users', 'UsersController');
