<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username', 50)->unique();
            $table->string('email', 100)->unique();
            $table->string('names', 50);
            $table->string('paternal_surname', 50);
            $table->string('maternal_surname', 50)->nullable();
            $table->integer('age')->nullable();
            $table->unsignedBigInteger('role');
            $table->foreign('role')->references('id')->on('Roles')->onDelete('cascade');
            $table->text('permissions', 100);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
